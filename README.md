# Telegram 4 MQL

This is the public repository for Telegram 4 MQL. It is not meant as a code repository as Telegram 4 MQL is not (yet) open source. This repository is for issue tracking and feature requesting only.

## What is it about?
**Telegram 4 MQL** is a .NET library that brings all the possibilities from the well known [Telegram Messenger](https://telegram.me) to Metatrader. With the help of the Telegram Bot feature you will be able to send and receive messages with ease right from your MQL code.

Sample use cases of that might be for example:
* Get a notification whenever an expert advisor makes a trade.
* Receive scheduled messages with screenshots of the charts on your screen.
* Remotely instruct your expert advisor to go out of a trade.

## Want to get deeper into it?
Then simply visit the projects [homepage](telegram4mql.steven-england.info "telegram4mql.steven-england.info") or [documentation](telegram4mql.steven-england.info/doc "telegram4mql.steven-england.info/doc").

## Feature requests and bug reports
If you're facing any problems concerning Telegram 4 MQL or just want to tell improvement proposals, feel free to open an issue right here in this repository.
You can also write a mail to *contact(at)steven-england.info*.